<System.ComponentModel.RunInstaller(True)> Partial Class ProjectInstaller
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.OCDFClientProcessInstaller = New System.ServiceProcess.ServiceProcessInstaller
        Me.OCDFClientInstaller = New System.ServiceProcess.ServiceInstaller
        Me.OCDFClientServiceController = New System.ServiceProcess.ServiceController
        '
        'OCDFClientProcessInstaller
        '
        Me.OCDFClientProcessInstaller.Password = Nothing
        Me.OCDFClientProcessInstaller.Username = Nothing
        '
        'OCDFClientInstaller
        '
        Me.OCDFClientInstaller.Description = "Service to download newsmessages from OBI"
        Me.OCDFClientInstaller.DisplayName = "NTB OCDF-Client"
        Me.OCDFClientInstaller.ServiceName = "NTB OCDF-client"
        '
        'OCDFClientServiceController
        '
        Me.OCDFClientServiceController.ServiceName = "OCDFClient"
        '
        'ProjectInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.OCDFClientProcessInstaller, Me.OCDFClientInstaller})

    End Sub
    Friend WithEvents OCDFClientProcessInstaller As System.ServiceProcess.ServiceProcessInstaller
    Friend WithEvents OCDFClientInstaller As System.ServiceProcess.ServiceInstaller
    Friend WithEvents OCDFClientServiceController As System.ServiceProcess.ServiceController

End Class
