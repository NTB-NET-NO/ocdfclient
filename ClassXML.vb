Imports system
Imports System.IO
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Text
Imports OCDFclient
Imports ntb_FuncLib
Imports OCDFclient.classFile
Imports OCDFclient.classTime

Public Class ClassXML
    ' -----------------------------------------------------------------------------
    ' ClassXML
    '
    ' Description:
    '   This class controlls everything that has to do with reading and writing to the 
    '   state XML-file 
    '
    ' Functions/Subroutines:
    '   readValueFromXML = This subroutine reads values from the XML-file
    '   readXML = This subroutine reads from the XML-file
    '   WriteStateToXMLFile = This subroutine shall use the groupname to find the value and change the value
    '   ShowXmlNode = This function writes to logfile what we get out of the reader... Not sure if this function is in use anymore.
    '   WriteStateXMLFile = This subroutine shall write the StateXMLFile 
    '   checkAgeOfLastArticle = This routine checks if the file is older than 30 minutes. If it is we shall restart the service
    '
    ' Returns:
    '   Groupname returns the value of the Groupname in the XML.
    '
    ' Notes :
    '   none
    ' -----------------------------------------------------------------------------


    ' Denne klassen skal brukes for � holde orden p� lesing og skriving til xml-filen som 
    ' holder orden p� hva som var siste nummer som ble skrevet til filen
    ' Dette fordi vi �nsker � finne ut n�r det ikke skjer noen endringer i filen.
    ' Dette fordi henting fra AP plutselig stopper av en eller annen grunn. 

    '<?xml version="1.0" encoding="utf-8"?>
    '<state>
    ' <group
    '  name="c:\utvikling\APNewsReceiver\log\ap.ds.dsf.all.log"
    ' value="1459120" />
    '</state>
    '
    ' for OCDF-l�sningen legger vi ogs� til timestamp. S� da vil denne se slik ut:
    '<?xml version="1.0" encoding="utf-8"?>
    '<state>
    ' <file name="c:\utvikling\APNewsReceiver\log\state.log" ' value="1459120" timestamp = "2008080812341234" />
    '</state>
    ' 
    '
    '
    Private Shared appPath As String = My.Application.Info.DirectoryPath
    Public Shared grpName As String = Nothing
    Public Shared msgNum As String = Nothing

    Public Function readOCDFValueFromXML(ByRef strXMLseqid As String, ByRef strXMLTimestamp As String) As Boolean
        ' -----------------------------------------------------------------------------
        ' readValueFromXML
        '
        ' Description:
        '   This subroutine reads values from the XML-file. 
        '
        ' Parameters:
        '   Groupname = This is the name of the group that we shall read values from. 
        '
        ' Returns:
        '   Groupname returns the value of the Groupname in the XML.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------

        ' APNewsService.WriteLogFile("In readValueFromXML")
        Dim docXML As New XmlDocument
        Dim strXML As String = Nothing


        docXML.Load(appPath & "\" & OCDF.xmlStateFile)

        Dim strRoot As XmlNode = docXML.DocumentElement
        Dim strGroup As XmlNode = Nothing
        Dim strXPath As String = "//state/file[@name='OCDF']"




        Try
            strGroup = strRoot.SelectSingleNode(strXPath)
        Catch xpathex As System.Xml.XPath.XPathException
            OCDF.WriteLogFile("*** XPath Exception ***")
            OCDF.WriteLogFile(xpathex.ToString)
            Return False
        Catch ex As Exception
            OCDF.WriteLogFile(ex.ToString)
            Return False
        End Try

        Try
            strXMLseqid = strGroup.Attributes(1).Value.ToString
        Catch ex As Exception
            OCDF.WriteLogFile(ex.ToString)
            Return False
        End Try

        Try
            strXMLTimestamp = strGroup.Attributes(2).Value.ToString
        Catch ex As Exception
            OCDF.WriteLogFile(ex.ToString)
            Return False
        End Try

        If Not strXMLseqid.ToString = Nothing And Not strXMLTimestamp = Nothing Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function readValueFromXML(ByVal GroupName As String)
        ' -----------------------------------------------------------------------------
        ' readValueFromXML
        '
        ' Description:
        '   This subroutine reads values from the XML-file. 
        '
        ' Parameters:
        '   Groupname = This is the name of the group that we shall read values from. 
        '
        ' Returns:
        '   Groupname returns the value of the Groupname in the XML.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------

        ' APNewsService.WriteLogFile("In readValueFromXML")
        Dim docXML As New XmlDocument
        Dim strXML As String = Nothing
        Dim strXMLReturnString As String = Nothing
        docXML.Load(appPath & "\" & OCDF.xmlStateFile)

        Dim strRoot As XmlNode = docXML.DocumentElement
        Dim strGroup As XmlNode = Nothing
        Dim strXPath As String = "//state/group[@name='" & GroupName & "']"




        Try
            strGroup = strRoot.SelectSingleNode(strXPath)
        Catch xpathex As System.Xml.XPath.XPathException
            OCDF.WriteLogFile("*** XPath Exception ***")
            OCDF.WriteLogFile(xpathex.ToString)
        Catch ex As Exception
            OCDF.WriteLogFile(ex.ToString)
        End Try

        Try
            strXMLReturnString = strGroup.Attributes(1).Value.ToString
        Catch ex As Exception
            OCDF.WriteLogFile(ex.ToString)
        End Try

        If Not strXMLReturnString.ToString = Nothing Then
            Return strXMLReturnString.ToString
        Else
            Return 0
        End If

    End Function
    
    
    Public Sub readXML()
        ' -----------------------------------------------------------------------------
        ' readXML
        '
        ' Description:
        '   This subroutine reads from the XML-file. 
        '
        ' Parameters:
        '   none
        '
        ' Returns:
        '   Nothing.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        ' APNewsService.WriteLogFile("In readXML")
        Dim myXMLSettings As New XmlReaderSettings
        Dim settingsReader As XmlReader = _
            XmlReader.Create(appPath & "\" & OCDF.xmlStateFile, myXMLSettings)
        Try


            While settingsReader.Read
                ' Process node here
                ShowXmlNode(settingsReader)
                While settingsReader.MoveToNextAttribute
                    ' Process node here
                    ShowXmlNode(settingsReader)
                End While
            End While
        Catch ex As Exception
            OCDF.WriteLogFile(ex.ToString)
        End Try
        OCDF.WriteLogFile("Finished readXML")
        settingsReader.Close()



    End Sub

    Public Sub WriteOCDFStateToXmlFile(ByVal seqid As String, ByVal TimeStamp As String)
        ' -----------------------------------------------------------------------------
        ' WriteStateToXMLFile
        '
        ' Description:
        '   This subroutine shall use the groupname to find the value and change the value.
        '   Then it shall save the changed value
        '
        '   
        '
        ' Parameters:
        '   groupname is not used in OCDF
        '   groupname = the groupname that we shall write into the xmlfile.
        '   value = the value we shall asign to the groupname
        '   timestamp = value of the time the file was there. 
        '
        ' Returns:
        '   Nothing.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------

        Dim docXML As New XmlDocument
        Dim strXML As String = Nothing

        docXML.Load(appPath & "\" & OCDF.xmlStateFile)

        Dim strRoot As XmlNode = docXML.DocumentElement
        Dim strGroup As XmlNode = Nothing
        Dim strXPath As String = "//state/file[@name='OCDF']"



        Try
            strGroup = strRoot.SelectSingleNode(strXPath)
        Catch xpathex As System.Xml.XPath.XPathException
            OCDF.WriteLogFile("In WriteStateToXMLFile: Selecting single node")
            OCDF.WriteLogFile("*** XPath Exception ***")
            OCDF.WriteLogFile(xpathex.ToString)
        Catch ex As Exception
            OCDF.WriteLogFile(ex.ToString)
        End Try

        Try
            Dim str As String = strGroup.Attributes(1).Value.ToString
        Catch ex As Exception
            OCDF.WriteLogFile(ex.ToString)
        End Try

        Try
            strGroup.Attributes(1).Value = seqid.ToString
        Catch ex As Exception
            OCDF.WriteLogFile("In WriteStateToXMLFile: Getting value")
            OCDF.WriteLogFile(ex.ToString)
        End Try

        Try
            strGroup.Attributes(2).Value = TimeStamp.ToString
        Catch ex As Exception
            OCDF.WriteLogFile("In WriteStateToXMLFile: Getting value")
            OCDF.WriteLogFile(ex.ToString)
        End Try

        Try
            docXML.Save(appPath & "\" & OCDF.xmlStateFile)
        Catch ex As Exception
            OCDF.WriteLogFile("In WriteStateToXMLFile: Saving XML-file")
            OCDF.WriteLogFile(ex.ToString)
        End Try
    End Sub
    Public Sub WriteStateToXMLFile(ByVal groupname As String, ByVal value As String)

        ' -----------------------------------------------------------------------------
        ' WriteStateToXMLFile
        '
        ' Description:
        '   This subroutine shall use the groupname to find the value and change the value.
        '   Then it shall save the changed value
        '
        '   
        '
        ' Parameters:
        '   groupname = the groupname that we shall write into the xmlfile.
        '   value = the value we shall asign to the groupname
        '
        ' Returns:
        '   Nothing.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------

        Dim docXML As New XmlDocument
        Dim strXML As String = Nothing

        docXML.Load(appPath & "\" & OCDF.xmlStateFile)

        Dim strRoot As XmlNode = docXML.DocumentElement
        Dim strGroup As XmlNode = Nothing
        Dim strXPath As String = "//state/group[@name='" & groupname & "']"



        Try
            strGroup = strRoot.SelectSingleNode(strXPath)
        Catch xpathex As System.Xml.XPath.XPathException
            OCDF.WriteLogFile("In WriteStateToXMLFile: Selecting single node")
            OCDF.WriteLogFile("*** XPath Exception ***")
            OCDF.WriteLogFile(xpathex.ToString)
        Catch ex As Exception
            OCDF.WriteLogFile(ex.ToString)
        End Try

        Try
            Dim str As String = strGroup.Attributes(1).Value.ToString
        Catch ex As Exception
            OCDF.WriteLogFile(ex.ToString)
        End Try

        Try
            strGroup.Attributes(1).Value = value.ToString
        Catch ex As Exception
            OCDF.WriteLogFile("In WriteStateToXMLFile: Getting value")
            OCDF.WriteLogFile(ex.ToString)
        End Try

        Try
            docXML.Save(appPath & "\" & OCDF.xmlStateFile)
        Catch ex As Exception
            OCDF.WriteLogFile("In WriteStateToXMLFile: Saving XML-file")
            OCDF.WriteLogFile(ex.ToString)
        End Try



    End Sub

    Public Sub WriteOCDFStateFile()
        ' -----------------------------------------------------------------------------
        ' WriteStateXMLFile
        '
        ' Description:
        '   This subroutine shall write the StateXMLFile 
        '   This is done only when the program is initiated, so that we get the correct data in. 
        '   What is shall do is do a replace in the xml-file. 
        '   
        '
        ' Parameters:
        '   value = the value we shall asign to the groupname
        '   timestamp = the time when the data was written to the file. 
        '
        ' Returns:
        '   Nothing.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------



        Dim myXMLsettings As New XmlDocument
        Dim myXMLDeclare As XmlDeclaration

        Dim docState As Xml.XmlElement
        Dim OCDFstate As Xml.XmlElement
        OCDF.WriteLogFile("Deletes the XML-file")

        ' We delete the XML-file that controls the state of the file. 
        If (My.Computer.FileSystem.FileExists(appPath & "\" & OCDF.xmlStateFile)) Then
            Try
                Kill(appPath & "\" & OCDF.xmlStateFile)
            Catch ex As Exception
                OCDF.WriteLogFile(ex.ToString)
            End Try

        End If



        ' start the XML document ith an XML declaration
        myXMLsettings = New Xml.XmlDocument
        myXMLDeclare = myXMLsettings.CreateXmlDeclaration("1.0", Nothing, String.Empty)

        myXMLsettings.InsertBefore(myXMLDeclare, myXMLsettings.DocumentElement)

        ' Add the root <state> element
        docState = myXMLsettings.CreateElement("state")
        myXMLsettings.InsertAfter(docState, myXMLDeclare)


        '        Dim b As Integer

        ' This part is not needed.
        'For b = 0 To groupname.Count - 1
        'nntpGroup = myXMLsettings.CreateElement("group")
        'nntpGroup.SetAttribute("name", groupname(b).ToString)
        'nntpGroup.SetAttribute("last", "0")
        'docState.AppendChild(nntpGroup)
        'Next b

        ' We add to the statefile
        OCDFstate = myXMLsettings.CreateElement("file")
        OCDFstate.SetAttribute("name", "OCDF")
        OCDFstate.SetAttribute("last", "0")
        OCDFstate.SetAttribute("timestamp", "0")
        docState.AppendChild(OCDFstate)


        ' We write to the statefile
        OCDF.WriteLogFile("We will now try and save the file to " & appPath & "\" & OCDF.xmlStateFile)
        Try
            myXMLsettings.Save(appPath & "\" & OCDF.xmlStateFile)
        Catch ex As Exception
            OCDF.WriteLogFile("In WriteStateXMLFile: Save XML File")
            OCDF.WriteLogFile("We could not save the file. The exception was: ")
            OCDF.WriteLogFile(ex.ToString)
        End Try

    End Sub

    Public Sub WriteStateXMLFile(ByVal groupname As ArrayList)
        ' -----------------------------------------------------------------------------
        ' WriteStateXMLFile
        '
        ' Description:
        '   This subroutine shall write the StateXMLFile 
        '   This is done only when the program is initiated, so that we get the correct data in. 
        '   What is shall do is do a replace in the xml-file. 
        '   
        '
        ' Parameters:
        '   groupname = the groupname that we shall write into the xmlfile.
        '   value = the value we shall asign to the groupname
        '   timestamp = the time when the data was written to the file. 
        '
        ' Returns:
        '   Nothing.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------



        Dim myXMLsettings As XmlDocument
        Dim myXMLDeclare As XmlDeclaration

        Dim docState As Xml.XmlElement
        Dim nntpGroup As Xml.XmlElement
        OCDF.WriteLogFile("Deletes the XML-file")
        If (My.Computer.FileSystem.FileExists(appPath & "\" & OCDF.xmlStateFile)) Then
            Try
                Kill(appPath & "\" & OCDF.xmlStateFile)
            Catch ex As Exception
                OCDF.WriteLogFile(ex.ToString)
            End Try

        End If



        ' start the XML document ith an XML declaration
        myXMLsettings = New Xml.XmlDocument
        myXMLDeclare = myXMLsettings.CreateXmlDeclaration("1.0", Nothing, String.Empty)

        myXMLsettings.InsertBefore(myXMLDeclare, myXMLsettings.DocumentElement)

        ' Add the root <state> element
        docState = myXMLsettings.CreateElement("state")
        myXMLsettings.InsertAfter(docState, myXMLDeclare)


        Dim b As Integer
        For b = 0 To groupname.Count - 1
            nntpGroup = myXMLsettings.CreateElement("group")
            nntpGroup.SetAttribute("name", groupname(b).ToString)
            nntpGroup.SetAttribute("last", "0")
            docState.AppendChild(nntpGroup)
        Next b
        OCDF.WriteLogFile("We will now try and save the file to " & appPath & "\" & OCDF.xmlStateFile)
        Try
            myXMLsettings.Save(appPath & "\" & OCDF.xmlStateFile)
        Catch ex As Exception
            OCDF.WriteLogFile("In WriteStateXMLFile: Save XML File")
            OCDF.WriteLogFile("We could not save the file. The exception was: ")
            OCDF.WriteLogFile(ex.ToString)
        End Try



    End Sub

    Public Shared Sub ShowXmlNode(ByVal reader As XmlReader)
        ' -----------------------------------------------------------------------------
        ' ShowXmlNode
        '
        ' Description:
        '   This function writes to logfile what we get out of the reader... Not sure if this 
        '   function is in use anymore.
        '
        ' Parameters:
        '   reader = the XMLreader
        '
        ' Returns:
        '   Nothing.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------

        'If reader.Depth > 0 Then
        'For depthcount As Integer = 1 To reader.Depth
        'APNewsService.WriteLogFile(" ")
        'Next
        'End If

        If reader.Name = "name" Then
            grpName = reader.Value.ToString
            OCDF.WriteLogFile(grpName)
        End If

        If reader.Name = "value" Then
            msgNum = reader.Value.ToString
            OCDF.WriteLogFile(msgNum)
        End If

        If reader.Name = "timestamp" Then
            msgNum = reader.Value.ToString
            OCDF.WriteLogFile(msgNum)
        End If

    End Sub

    

    


End Class
