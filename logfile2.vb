Imports System.Configuration.ConfigurationSettings
Imports System.Configuration
Imports System.Data.OleDb
Imports System.Text
Imports System.IO
Imports System.Net.Mail


Public Class LogFile
    ' -----------------------------------------------------------------------------
    ' LogFile
    '
    ' Description: This class handles everything that has to do with logging of
    '   NTB-applications.
    '
    ' Parameters:
    '
    ' Returns:
    '
    ' Notes :
    '   none
    ' -----------------------------------------------------------------------------
    Private Shared errorMailAddress As String = My.Settings.errorMailAddress
    Private Shared myEncoding As Encoding = Encoding.GetEncoding("iso-8859-1")

    Private Shared SMTPServer As String = My.Settings.SMTPServer
    Private Shared emailRcpt As String = My.Settings.emailRcpt
    Public Shared logFilePath As String = My.Settings.logFilePath
    Public Shared lFileName As String = My.Settings.logFileName
    Public Shared logFileName As String = logFilePath & "\" & lFileName



    Public Shared Sub WriteErr(ByRef strLogPath As String, ByRef strMessage As String, ByRef e As Exception)
        ' -----------------------------------------------------------------------------
        ' WriteErr
        '
        ' Description:
        '   This routine writes the error that occured in the logfile
        '
        ' Parameters:
        '   strLogPath = The path to the logfile
        '   strMessage = The errormessage
        '
        ' Returns:
        '   Nothing, but if it is stated that the application should send
        '   the errormessage via mail, it will call the mail routine
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        Dim strLine As String

        On Error Resume Next

        strLine = "Error: " & Format(Now, "yyyy-MM-dd HH:mm:ss") & vbCrLf
        strLine &= strMessage & vbCrLf
        strLine &= e.Message & vbCrLf
        strLine &= e.Source & vbCrLf
        strLine &= e.StackTrace

        Dim strFile As String = strLogPath & "\Error-" & Format(Now, "yyyy-MM-dd") & ".log"
        Dim w As New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strLine)
        w.WriteLine("------------------------------------------------------------")
        w.Flush()  ' update underlying file
        w.Close()  ' close the writer and underlying file

        If errorMailAddress <> "" Then
            SendErrMail(strLine)
        End If
    End Sub

    Public Shared Sub WriteDebug(ByRef strLogPath As String, ByRef strMessage As String)
        ' -----------------------------------------------------------------------------
        ' WriteDebug
        '
        ' Description:
        '   Writes debug information provided in the routine call to the logfile
        '
        ' Parameters:
        '   strLogPath = The logfile to write message to
        '   strMessage = The message to write to the logfile.
        '   If we are in debugmode it shall also write to the debug-window.
        '
        ' Returns:
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        Dim strLine As String = "Debug: " & Format(Now, "yyyy-MM-dd HH:mm:ss") & ": " & strMessage
        Dim strFile As String = strLogPath
        Dim w As New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strLine)
        w.Flush()  ' update underlying file
        w.Close()  ' close the writer and underlying file

#If DEBUG Then
        Debug.WriteLine(strLine)
#End If
    End Sub

    Public Shared Sub WriteLog(ByRef strLogPath As String, ByRef strMessage As String)
        ' -----------------------------------------------------------------------------
        ' WriteLog
        '
        ' Description:
        '   Writes the message provided in the routine call to the logfile
        '
        ' Parameters:
        '   strLogPath = The logfile to write message to
        '   strMessage = The message to write to the logfile.
        '
        ' Returns:
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        Dim strLine As String = Format(Now, "yyyy-MM-dd HH:mm:ss") & ": " & strMessage
        Dim strFile As String = strLogPath
        Dim w As StreamWriter = New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strLine)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Shared Sub WriteLogNoDate(ByRef strLogPath As String, ByRef strMessage As String)
        ' -----------------------------------------------------------------------------
        ' WriteLogNoDate
        '
        ' Description:
        '   This writes messages to the logfile, but not the date and time
        '
        '
        ' Parameters:
        '   strLogPath = The logfile to write message to
        '   strMessage = The message to write to the logfile.
        '
        ' Returns:
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------

        Dim strFile As String = strLogPath
        Dim w As New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strMessage)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Shared Sub WriteFile(ByRef strFileName As String, ByRef strContent As String, Optional ByVal append As Boolean = False)
        ' -----------------------------------------------------------------------------
        ' WriteFile
        '
        ' Description:
        '   This writes a file containing the string provided in the function call
        '   This function is used for ........
        '
        ' Parameters:
        '   strFilename = The filename to write to
        '   strContent = The content of the file
        '   append =    boolean value. 
        '               if false I would expect that it writes to a new file
        '               if true it adds to the file stated. 
        '
        ' Returns:
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------

        Dim w As New StreamWriter(strFileName, append, myEncoding)
        w.WriteLine(strContent)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Shared Function ReadFile(ByVal strFileName As String) As String
        ' -----------------------------------------------------------------------------
        ' ReadFile
        '
        ' Description:
        '   This Function reads the content of a file
        '
        ' Parameters:
        '   strFileName = The filename to read from
        '
        ' Returns:
        '   ReadFile = The content of the file.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------

        ' Simple File reader returns File as String
        Dim sr As New StreamReader(strFileName, myEncoding)    ' File.OpenText("log.txt")
        ReadFile = sr.ReadToEnd
        sr.Close()
    End Function

    Public Shared Sub MakePath(ByVal strPath As String)
        ' -----------------------------------------------------------------------------
        ' MakePath
        '
        ' Description:
        '   Creates a directory
        '
        ' Parameters:
        '   strPath = the directory to be created
        '
        ' Returns:
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        'If Not Directory.Exists(strPath) Then
        Directory.CreateDirectory(strPath)
        'End If
    End Sub

    Public Shared Function MakeSubDirDate(ByRef strFilePath As String, ByVal dtTimeStamp As DateTime) As String
        ' -----------------------------------------------------------------------------
        ' MakeSubDirDate
        '
        ' Description:
        '   This creates a directory that also contains the current date
        '
        ' Parameters:
        '   strFilePath = Directory
        '   dtTimeStamp = The date
        '
        ' Returns:
        '   The complete name of the directory that has been created
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        Dim strSub As String = Format(dtTimeStamp, "yyyy-MM") & "\" & Format(dtTimeStamp, "yyyy-MM-dd")

        Dim strPath As String = Path.GetDirectoryName(strFilePath) & "\" & strSub & "\"

        ' Checks if the directory exists. If it does not we will create it
        If Not Directory.Exists(strPath) Then
            Directory.CreateDirectory(strPath)
        End If

        Return strPath & Path.GetFileName(strFilePath)

    End Function

    Private Shared Sub SendErrMail(ByVal strMessage As String)
        ' -----------------------------------------------------------------------------
        ' SendErrMail
        '
        ' Description:
        '   If the configuration says that there shall be sent an email error message
        '   this Routine will do so.
        '
        ' Parameters:
        '   strMessage = The message to be sent.
        '
        ' Returns:
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------

        Dim emailSender As SmtpClient
        Dim theMessage As MailMessage

        ' connect to the server-
        emailSender = New System.Net.Mail.SmtpClient(SMTPServer)

        ' Creates the mail message text
        theMessage = New MailMessage
        theMessage.From = New MailAddress("505@ntb.no")
        theMessage.To.Add(emailRcpt)
        theMessage.Subject = "Feil i AP Service!"

        theMessage.Body = "Det har oppst�tt en feil i AP Service." & vbCrLf & vbCrLf
        theMessage.Body = theMessage.Body & "Tid: " & Now & vbCrLf
        theMessage.Body = theMessage.Body & "Feilmelding: " & strMessage & vbCrLf & vbCrLf

        Try
            emailSender.Send(theMessage)
        Catch e As Exception
            LogFile.WriteDebug(logFileName, "FAILED to send warning by e-Mail!")
            LogFile.WriteErr(logFileName, "", e)
        End Try
    End Sub

End Class
