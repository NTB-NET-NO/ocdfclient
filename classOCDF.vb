Imports system.Net.Sockets
Imports System.IO
Imports System.Configuration.ConfigurationSettings
Imports System.Configuration
Imports OCDFclient
Imports OCDFclient.classXML
Imports OCDFclient.classFile
Imports OCDFclient.classTime
Imports System.Globalization

Public Class classOCDF


    ' S_CONNECT_REQcrosoft Telnet Client
    ' S_ACK;pid=17121 state=AWAIT_CONNECTION_REQUEST event=S_CONNECT_REQ (9)
    ' S_LOGON_REQ;NTB;5t6y7$e
    ' S_ACK;AWAIT_COMMAND_START
    ' S_CMD_START_REQ;REALTIME;1
    ' S_ENQ_REQ;1;pid=17121 state=AWAIT_COMMAND_END event=I_TIMEOUT (13)

    ' The default TCP/IP port number for OCDF is 9400 for prod and 9401 for test.
    ' I'll put this in the config.file. 
    Public Port As Integer = My.Settings.ServerPort
    Public Messages As Integer = 0                  ' This is not in use
    Public errorCode As Integer = 0                 ' This is not in use
    Public lastArticle As Integer = 0               ' This is not in use
    Public firstArticle As Integer = 0              ' This is not in use
    Public debugString As String = Nothing
    Public OCDFerrorCode As Integer = 0


    ' These NNTP-spesific return codes and can be deleted from this part
    Private Const CommandFailure As String = "-ERR" ' This does not apply for NNTP-servers, but for emailservers.
    Private Const GroupFailure As String = "411" ' No such newsgroup
    Private Const CommandSuccess As String = "211" ' using this instead of an error... everything else are errors

    Private OCDFServer As TcpClient
    Private CommandSender As NetworkStream
    Private ContentReceiver As StreamReader

    Public serviceRunning As Boolean = False

    Public Shared logFilePath As String = My.Settings.logFilePath

    Public Shared logFileName As String = logFilePath & "\" & OCDF.datefilename & My.Settings.logFileName

    Public Sub OCDFConnect(ByVal serverName As String)
        ' -----------------------------------------------------------------------------
        ' Connect
        '
        ' Description:
        '   This routine connects to the NNTP-server using the servername, username 
        '   and password provided in the routine-call.
        '
        ' Parameters:
        '   serverName = The name of the NNTP-server. This is stored in app.config
        '   userName = The username of the user. This is stored in app.config
        '   passWord = the password of the user. This is stored in app.config
        '
        ' Returns:
        '   Nothing
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        Dim commandData As String
        Dim contentBuffer() As Byte
        Dim responseString As String = Nothing

        serviceRunning = True

        ' Connect to the NNTP-server
        Try
            If My.Settings.debug = True Then
                OCDF.WriteLogFile("Kobler til " & serverName & " port: " & My.Settings.ServerPort)
            End If
            OCDFServer = New TcpClient(serverName, My.Settings.ServerPort)
            CommandSender = OCDFServer.GetStream()
            ContentReceiver = New StreamReader(CommandSender)

        Catch
            Throw
        End Try


        ' Drar igjennom brukernavn og passord
        If Not responseString = Nothing Then
            responseString = Nothing
        End If

        Try
            ' We the S_CONNECT_REQ command to tell that we are to connect to the OBI server
            commandData = My.Settings.CommandConnect & vbCrLf
            If My.Settings.debug = True Then
                OCDF.WriteLogFile("SENDER S_CONNECT_REQ kommandoen")
            End If
            contentBuffer = System.Text.Encoding.ASCII.GetBytes(commandData.ToCharArray())
            CommandSender.Write(contentBuffer, 0, contentBuffer.Length)
            responseString = ContentReceiver.ReadLine()
            If My.Settings.debug = True Then
                OCDF.WriteLogFile("RESULTAT av S_CONNECT_REQ: " & responseString)
            End If
        Catch ex As System.Net.Sockets.SocketException
            OCDF.WriteLogFile("***** CONNECTION ERROR *****")
            OCDF.WriteErrorLogFile("Error happened in classOCDF.vb Connect. Connection failure", ex.Message)
            OCDF.WriteErrorLogFile("Connection failure: ", ex.Message)
            OCDF.WriteErrorLogFile("Socket errorcode: ", ex.SocketErrorCode.ToString)
            OCDF.WriteErrorLogFile("Native errorcode: ", ex.NativeErrorCode.ToString)
            If Not ex.InnerException Is Nothing Then
                OCDF.WriteErrorLogFile("Inner Exception Message: ", ex.InnerException.Message)
            End If

        Catch exs As Exception
            OCDF.WriteErrorLogFile("Normal Exception connection failure (in nntp.vb Connect): ", exs.Message)
            If Not exs.InnerException Is Nothing Then
                OCDF.WriteErrorLogFile("Inner Exception Message: ", exs.InnerException.Message)
            End If

        End Try

        If Not responseString = Nothing Then
            responseString = Nothing
        End If




    End Sub

    Function OCDFLogon() As String
        Dim commandData As String
        Dim contentBuffer() As Byte
        Dim responseString As String = Nothing

        '  Sending logon command. 
        Try
            If My.Settings.debug = True Then
                OCDF.WriteLogFile("Sender kommandoen S_LOGON_REQ;" & My.Settings.Username & ";" & My.Settings.Password)
            End If
            commandData = "S_LOGON_REQ;" & My.Settings.Username & ";" & My.Settings.Password & vbCrLf
            contentBuffer = System.Text.Encoding.ASCII.GetBytes(commandData.ToCharArray())
            CommandSender.Write(contentBuffer, 0, contentBuffer.Length)
            responseString = ContentReceiver.ReadLine()
            If My.Settings.debug = True Then
                OCDF.WriteLogFile("Result of responsestring: " & responseString)
            End If
        Catch ex As System.Net.Sockets.SocketException
            OCDF.WriteLogFile("SocketException:" & ex.Message.ToString)

        Catch exs As Exception
            OCDF.WriteLogFile("Exception in OCDFLogon:" & exs.Message.ToString)
        End Try
        Return responseString

    End Function

    Function OCDFCommands(ByRef strLine As String, ByRef textReader As TextReader) As Boolean
        ' -----------------------------------------------------------------------------
        ' OCDFCommands
        '
        ' Description:
        '   The idea behind this subroutine / function (not sure which one yet) is that we send the line we receive to this
        '   function / routine where we check if the line includes some OCDF-commands. 
        '   We can then switch on which command requests we get from the OCDF-server and send the appropriate OCDF-command back.
        '
        ' Parameters:
        '   strLine: A string which contains the line we receive from the OCDF-server.
        ' Returns:
        '   Not sure if this code shall return anything. 
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        Dim strCommando As String = Nothing
        Dim contentBuffer() As Byte
        Dim strCommandos(2) As String



        With strLine
            If .StartsWith("S_ABORT;") Then
                ' We have gotten the S_ABORT command which means that we shall reboot.
                ' This should mean that we shall return some kind of code. 
                Return False
            End If

            If .StartsWith("S_ACK;AWAIT_COMMAND_END") Then
                ' Not sure what this Server-request means or if we shall return something.
                Return False
            End If

            If .StartsWith("S_CMD_END_REQ;") Then
                ' This line means that we have gotten a S_CMD_END_REQ-command. And we shall use this one to
                ' return this code: S_ENQ_ACK; (and the number which is stated after the semicolon. 
                strCommandos(0) = "S_ACK"
            End If

            If .StartsWith("S_ENQ_REQ") Then
                If My.Settings.debug = True Then
                    OCDF.WriteLogFile("Vi skal fortelle at vi skal holde liv i denne beaten")
                End If
                strCommandos(0) = "S_ENQ_ACK;" & OCDFGetParamId(strLine)
            End If

            If .StartsWith("S_NTB_ABORT") Then
                If My.Settings.debug = True Then
                    OCDF.WriteLogFile("Vi forteller at vi skal stoppe hentingen av denne meldingen (S_ABORT)")
                End If
                strCommandos(0) = "S_ABORT"

                strCommandos(1) = "S_CMD_START_REQ;REALTIME;" & OCDFGetLastSeqId().ToString ' & OCDFGetParamId(strLine)
            End If


            If .StartsWith("S_NTB_QUIT_ABORT") Then
                If My.Settings.debug = True Then
                    OCDF.WriteLogFile("Vi forteller at vi skal stoppe hentingen av denne meldingen (S_ABORT)")
                End If
                strCommandos(0) = "S_ABORT"
            End If

        End With


        Try
            If Not strCommandos(0) = Nothing Then
                For Each strCommando In strCommandos
                    If Not String.IsNullOrEmpty(strCommando) Then

                        If My.Settings.debug = True Then
                            OCDF.WriteLogFile("Vi sender kommandoen: " & strCommando)
                        End If

                        Dim strtcom = strCommando

                        strCommando &= vbCrLf

                        contentBuffer = System.Text.Encoding.ASCII.GetBytes(strCommando.ToCharArray())
                        CommandSender.Write(contentBuffer, 0, contentBuffer.Length)

                        If strtcom = "S_ACK" Then
                            ' The S_ACK command does just tell the server that we know that we now have gotten the
                            ' S_CMD_ -command. 
                            OCDF.WriteLogFile("We return true")
                            Return True
                        End If

                        strLine = textReader.ReadLine


                        If My.Settings.debug = True Then
                            OCDF.WriteLogFile("Vi fikk f�lgende svar fra serveren: " & strLine.ToString)
                        End If

                        If strLine.StartsWith("S_") Then
                            OCDFCommands(strLine, textReader)
                        End If
                        If My.Settings.debug = True Then
                            OCDF.WriteLogFile("Vi har f�lgende i strLine: " & strLine.ToString)
                        End If

                    End If
                Next
                OCDF.WriteLogFile("Vi returnerer true")

                Return True

            End If
        Catch exs As SocketException
            OCDF.WriteLogFile("SocketException: " & exs.Message)
        Catch ex As Exception
            OCDF.WriteLogFile("Exception: " & ex.Message)
        End Try

        OCDF.WriteLogFile("Vi returnerer false")
        Return False
    End Function
    Function OCDFGetLastSeqId() As Int32
        Dim intstateSeqId As Int32 = 1
        Dim stateSeqId As String = Nothing
        Dim stateTimeStamp As String = Nothing

        Dim xmlFile As New ClassXML

        If IO.File.GetLastWriteTime(My.Settings.HomeDir & "\" & My.Settings.StateFile).Date < DateTime.Now.Date Then

        Else
            ' Statefilen er fra dagens dato eller nyere. Vi m� da hente seq-id fra statefil og legge til 1 for 
            ' � hente neste artikkel / pressemelding. 

            If xmlFile.readOCDFValueFromXML(stateSeqId, stateTimeStamp) = True Then


                ' Sjekker at innholdet i stateseqid er et nummer og ikke bare bogus... 

                If IsNumeric(stateSeqId) Then

                    intstateSeqId = Convert.ToInt32(stateSeqId)
                    intstateSeqId = intstateSeqId + 1


                End If


            End If
        End If

        Return intstateSeqId

    End Function
    Sub OCDFFixedDataReceive()
        ' -----------------------------------------------------------------------------
        ' OCDFFixedDataReceive
        '
        ' Description:
        '   This function receives the result from the FIXED_DATA command from the OCDF-server 
        '   Data comes out one line after the other. 
        '   This feed will stop when it comes to the end of the stream. 
        '
        ' Parameters:
        '
        ' Returns:
        '   Returns the whole que of messages. 
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------

        Dim commandData As String
        Dim contentBuffer() As Byte
        Dim responseString As String

        Dim theMessage As New System.Text.StringBuilder
        Dim oneLine As String = Nothing
        Dim XMLfile As New ClassXML
        responseString = Nothing
        ' check for an invalid message
        ' Not adding this now

        Dim textReader As TextReader

        Dim File As New classFile
        Dim fileAge As String = Nothing
        Dim filearray As Array
        ' Legg til kode for � kj�re inn Data_FIX fra OBI. 

        Try
            fileAge = File.checkAgeofFile(My.Settings.OutPath & "\" & My.Settings.FixedDataFile & ".txt")
            If My.Settings.debug = True Then OCDF.WriteLogFile("Dette er alderen: " & fileAge.ToString)
            ' Denne kan komme ut med f.eks 00:52
            filearray = Split(fileAge.ToString, ":")
            If filearray(0).ToString < 59 Then
                OCDF.WriteLogFile("Vi trenger ikke � hente filen. Den er ikke gammel nok!")
                Exit Sub
            End If
        Catch ex As Exception
            OCDF.WriteLogFile("Vi fikk en feilmelding: " & ex.Message)
            OCDF.WriteErrorLogFile("Vi fikk f�lgende feilmelding", ex.Message)
        End Try

        Try
            ' The OCDF-protocol uses PUSH-technology which means that we log onto the server and 
            ' then we send one command and we get the data that is present out all day long - or for as
            ' long as we like.

            


            Dim intStateSeqId As Integer

            intStateSeqId = OCDFGetLastSeqId()

            If My.Settings.debug = True Then
                OCDF.WriteLogFile("Kommando vi sender etter � ha testet for tid: " & My.Settings.CommandFixedData)
            End If



            commandData = My.Settings.CommandFixedData & vbCrLf
            contentBuffer = System.Text.Encoding.ASCII.GetBytes(commandData.ToCharArray())
            CommandSender.Write(contentBuffer, 0, contentBuffer.Length)

            textReader = New StreamReader(ContentReceiver.BaseStream, System.Text.Encoding.GetEncoding("ISO-8859-1"), True)


            ' We loop through the lines we are getting. When we get to the line that has the 
            ' last line in the article we escape the loop. 
            ' I don't know if we should though.
            ' If Not (GetEndOfArticleChar) Then

            Do While (ContentReceiver.EndOfStream = False And serviceRunning)
                oneLine = textReader.ReadLine()

                If (My.Settings.debug = True) Then
                    OCDF.WriteLogFile("oneLine in loop: " & oneLine)
                End If

                ' When we get to the end of the file we get this line. 
                ' S_CMD_END_REQ;0;pid=18669 state=AWAIT_COMMAND_END newState=AWAIT_COMMAND_END_ACK
                ' We then have to send the S_ACK-command and escape the do-loop / try-stuff. 

                If oneLine.StartsWith("S_CMD_END_REQ") And oneLine.Contains("AWAIT_COMMAND_END") Then
                    OCDF.WriteLogFile("We are at the end of the receive-list. sending S_ACK")
                    If OCDFCommands(oneLine, textReader) = True Then
                        Exit Do
                        Exit Try
                    End If
                End If

                theMessage.AppendLine(oneLine)
            Loop
        Catch exs As SocketException
            OCDF.WriteLogFile("***** Socket Error: OCDFMessageReceive *****")
            OCDF.WriteLogFile("Error happened in classOCDF.vb OCDFFixedDataReceive. Message retrieval failed: " & exs.Message)
            If Not exs.InnerException Is Nothing Then
                OCDF.WriteLogFile("Inner exception: " & exs.InnerException.Message)
            End If

        Catch ex As Exception
            OCDF.WriteLogFile("***** ERROR: OCDFMessageReceive *****")
            OCDF.WriteLogFile("Error happened in classOCDF.vb OCDFFixedDataReceive. Message retrieval failed: " & ex.Message)

            ' If Not ex.InnerException Is Nothing Then
            OCDF.WriteLogFile("Inner exception: " & ex.InnerException.Message)
            ' End If


        End Try

        OCDF.WriteLogFile("Ferdig med � hente. Vi skriver til fixeddata.txt")
        OCDF.WriteLogFile(oneLine.ToString)
        OCDFWriteMessage(theMessage.AppendLine(oneLine).ToString, "FixedData")


    End Sub



    Sub OCDFMessageReceive()
        ' -----------------------------------------------------------------------------
        ' OCDFMessageReceive
        '
        ' Description:
        '   This function receives the message from the OCDF-server after we send the 
        '   REALTIME command. Data comes out one line after the other. 
        '   An article is finished parsing when it comes to the n; (next line data)
        '
        ' Parameters:
        '
        ' Returns:
        '   Returns the whole que of messages. 
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------

        Dim commandData As String
        Dim contentBuffer() As Byte
        Dim responseString As String

        Dim theMessage As New System.Text.StringBuilder
        Dim oneLine As String
        Dim XMLfile As New ClassXML
        responseString = Nothing
        ' check for an invalid message
        ' Not adding this now
        Dim stateSeqId As String = Nothing
        Dim stateTimeStamp As String = Nothing
        Dim dtprmTimeStamp As DateTime = Nothing
        Dim dtstateTimeStamp As DateTime = Nothing
        Dim dstateTimeStamp As Date = Nothing

        Dim arrDataLine As Array
        Dim prmID As String = 1
        Dim nID As String = 1
        Dim prmSeqId As String = 1
        Dim prmTimeStamp As String = 1

        Dim textReader As TextReader

      



        ' Legg til kode for � kj�re inn Data_FIX fra OBI. 

       

        ' Getting the age of the FixedData.txt file. 
        ' If it is less than one our old, we won't do this
        


        Try
            ' The OCDF-protocol uses PUSH-technology which means that we log onto the server and 
            ' then we send one command and we get the data that is present out all day long - or for as
            ' long as we like.

            If My.Settings.debug = True Then
                OCDF.WriteLogFile("Vi sjekker alderen p� fila!")
                OCDF.WriteLogFile("Alderen p� fila: " & IO.File.GetLastWriteTime(My.Settings.HomeDir & "\" & My.Settings.StateFile).Date.ToString & "!")
                OCDF.WriteLogFile("Now: " & DateTime.Now.Date.ToString)
            End If
            




            Dim intStateSeqId As Integer

            intStateSeqId = OCDFGetLastSeqId()

            If My.Settings.debug = True Then
                OCDF.WriteLogFile("Kommando vi sender etter � ha testet for tid: " & My.Settings.CommandReceiveMessages & intStateSeqId.ToString)
            End If



            commandData = My.Settings.CommandReceiveMessages & intStateSeqId.ToString & vbCrLf
            contentBuffer = System.Text.Encoding.ASCII.GetBytes(commandData.ToCharArray())
            CommandSender.Write(contentBuffer, 0, contentBuffer.Length)

            textReader = New StreamReader(ContentReceiver.BaseStream, System.Text.Encoding.GetEncoding("ISO-8859-1"), True)

            ' oneLine = ContentReceiver.ReadLine()
            oneLine = textReader.ReadLine()

            If My.Settings.debug = True Then
                OCDF.WriteLogFile("Result of oneLine: " & oneLine)
            End If

            ' I will move the code below to another function. 
            ' This so that we separate code that does something with the text from 
            ' code that only starts / stops the feed. 

            ' When we send the commando we get one line after the other. When we get the last line of the
            ' article we shall send the string to the function that creates the XML-file.
            ' We shall look for the end of article character. In OCDF this is a line that does not
            ' have an escaped newline character. All other newlines has an escaped one... 

            ' If we get the (dreaded) keep alive ( heartbeat ) command, we must send this command




            ' We loop through the lines we are getting. When we get to the line that has the 
            ' last line in the article we escape the loop. 
            ' I don't know if we should though.
            ' If Not (GetEndOfArticleChar) Then



            Do While (ContentReceiver.EndOfStream = False And serviceRunning)
                ' Hvis vi f�r en linje som starter med n; s� vet vi at vi har en datalinje
                ' Denne kan vi splitte opp i mer informasjon som vi kan lagre her og der.

                '                If oneLine.StartsWith("S_ABORT;") Then
                ' Dette betyr at vi har mottatt S_ABORT-kommandoen som kicker oss ut.
                ' Da starter vi bare klienten p� nytt.
                'OCDF.WriteLogFile("******* RESTARTING AFTER S_ABORT COMMAND RECEIVE *******")
                ' Marks out the line below since we aren't connected anyway.
                'Exit Sub
                'End If

                ' suggested fix: 04.04.08: 
                ' The line below comes when we have a non -5 means that this is not a tradeday - which 
                ' again could mean that we've come to the end of the day
                ' we will then wait for the number of minutes we have stated in the config-file and start again
                ' that is... if we do this. Currently the program is kicked out of the server every five minutes
                ' and we ask to get in every five minutes... :D


                'If oneLine.StartsWith("S_NAK;") Then
                '    Dim oneSplitter
                '    oneSplitter = Split(oneLine.ToString, ";")
                '    If oneSplitter(1) = "-5" Then
                '        ' we shall wait the number of minutes we get before we restart ourselves. 
                '        ' This means that there won't be any messages. 


                '    End If
                'End If
                ' S_NAK;-5;Unexpected message in current state;pid=18703 state=AWAIT_COMMAND_END_ACK


                If oneLine.StartsWith("n;") And Not oneLine.Contains("S_ENQ_REQ;") Then
                    ' Da er vi inne i datalinjen
                    arrDataLine = OCDFgetDataLine(oneLine.ToString)
                    nID = OCDFgetFromDataLine(arrDataLine, "n")
                    prmID = OCDFgetFromDataLine(arrDataLine, "NId")
                    prmTimeStamp = OCDFgetFromDataLine(arrDataLine, "t")

                    If My.Settings.debug = True Then
                        OCDF.WriteLogFile("Dette har vi n�: Pressemelding-ID: " & prmID & " og prmTimeStamp: " & prmTimeStamp)
                    End If

                ElseIf oneLine.Contains("S_ENQ_REQ;") And Not oneLine.StartsWith("S_") Then
                    OCDFCommands("S_NTB_ABORT", textReader)
                    Exit Sub
                End If


                ' Vi sjekker om meldingen ikke har \ p� slutten av meldingen. Har den ikke det
                ' s� er dette siste linjen i meldingen. 
                ' Men kan det v�re en annen grunn til at \ mangler p� slutten? 
                If Not oneLine.EndsWith(My.Settings.LineEndCharacter) Then
                    ' Da oppretter vi ny fil med id som navn til ocdf
                    ' Alt vi mottar skal dumpes r�tt til denne filen. 
                    ' lagre ID i state-fil. 
                    ' n;1055;Ns1;t20080304080004;URLnhttp://www.newsweb.no/index.jsp?messageId=204414;NLaNO;NId204414;NtMELDEPLIKTIG HANDEL;NTeMANDATORY NOTIFICATION OF TRADE;CId6051;iNO0003078107;OBId;NhKj�p av egne aksjer;Nl254;Nm\
                    ' Vi vet at f�rste linjen inneholder mye av det vi trenger.

                    ' Dersom denne meldingen ikke har \ p� slutten og starter med S_ s� skal vi 
                    ' ikke skrive den til fil. Men starter den ikke med S_ s� er dette
                    ' mest sannsynlig slutten p� filen og dermed skal vi skrive til filen med
                    ' samme navn som pressemelding-id.

                    If Not oneLine.Contains("S_ENQ_REQ;") And Not oneLine.StartsWith("S_") Then
                        ' Vi skriver til filen
                        OCDFWriteMessage(theMessage.AppendLine(oneLine).ToString, prmID.ToString)
                        ' Vi t�mmer strengen
                        theMessage.Length = 0 ' T�mmer denne strengen?
                        ' Og vi oppdaterer Statefilen med ny ID og timestamp (selv om det siste ikke er n�dvendig)
                        XMLfile.WriteOCDFStateToXmlFile(nID, prmTimeStamp)
                    ElseIf oneLine.Contains("S_ENQ_REQ;") And Not oneLine.StartsWith("S_") Then
                        OCDFCommands("S_NTB_ABORT;", textReader)
                        Exit Sub
                    End If



                    ' Hvis denne ID'n finnes, skal vi hoppe over/ikke skrive "artikkelen" til fil. 

                Else
                    ' Vi har funnet at linjen ikke sluttes med \ (escape-character) 
                    ' dermed skal vi legge til linjen i theMessage-strengen...

                    ' Vi skal ikke ha linjer som starter med S_
                    If Not oneLine.StartsWith("S_") Then

                        theMessage.AppendLine(oneLine)

                    End If


                End If

                If My.Settings.debug = True Then
                    OCDF.WriteLogFile("Etter en del sjekker er oneLine: " & oneLine.ToString)
                End If


                If OCDFCommands(oneLine, textReader) = False Then
                    oneLine = textReader.ReadLine()
                End If




            Loop

            If My.Settings.debug = True Then
                OCDF.WriteLogFile("Vi er ferdig med loop!!!")
            End If


        Catch exs As SocketException
            OCDF.WriteLogFile("***** Socket Error: OCDFMessageReceive *****")
            OCDF.WriteLogFile("Error happened in classOCDF.vb OCDFMessageReceive. Message retrieval failed: " & exs.Message)
            If Not exs.InnerException Is Nothing Then
                OCDF.WriteLogFile("Inner exception: " & exs.InnerException.Message)
            End If

        Catch ex As Exception
            OCDF.WriteLogFile("***** ERROR: OCDFMessageReceive *****")
            OCDF.WriteLogFile("Error happened in classOCDF.vb OCDFMessageReceive. Message retrieval failed: " & ex.Message)

            ' If Not ex.InnerException Is Nothing Then
            OCDF.WriteLogFile("Inner exception: " & ex.InnerException.Message)
            ' End If


        End Try

        ' return message
        ' Return theMessage.ToString

    End Sub

    Private Sub OCDFWriteMessage(ByVal strMessage As String, ByVal strFileName As String)
        ' -----------------------------------------------------------------------------
        ' saveAttachment
        '
        ' Description:
        '   This routine saves the now decoded string to a file.
        '
        ' Parameters:
        '   Content = the decoded string that was fetched from the NNTP-message
        '   filename = the filename to write to: Number + .xml
        '   outpath = Write to the path provided in the routine call
        '   logFileName = Which logfile to write to.
        '
        ' Returns:
        '   Nothing
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        ' lagrer filen... 
        Try
            If My.Settings.debug = True Then
                OCDF.WriteLogFile("Vi skriver til filen: " & My.Settings.OutPath & "\" & strFileName.ToString & ".txt")
            End If

            IO.File.WriteAllText(My.Settings.OutPath & "\" & strFileName.ToString & ".txt", strMessage.ToString, System.Text.Encoding.GetEncoding("ISO-8859-1"))

            ' Vi skriver innholdet i strengen til logfilen ogs�...
            If My.Settings.debug = True Then
                OCDF.WriteLogFile("******************************MELDING***********************")
                OCDF.WriteLogFile("*                                                          *")
                OCDF.WriteLogFile("*                                                          *")
                OCDF.WriteLogFile("*                Vi skriver ut pressemelding               *")
                OCDF.WriteLogFile("*                                                          *")
                OCDF.WriteLogFile("*                                                          *")
                OCDF.WriteLogFile("******************************MELDING***********************")
                OCDF.WriteLogFile("Vi har skrevet f�lgende til logfile: " & strMessage.ToString)
                OCDF.WriteLogFile("******************************MELDING***********************")
                OCDF.WriteLogFile("*                                                          *")
                OCDF.WriteLogFile("*                                                          *")
                OCDF.WriteLogFile("*                Vi har skrevet ut pressemelding           *")
                OCDF.WriteLogFile("*                                                          *")
                OCDF.WriteLogFile("*                                                          *")
                OCDF.WriteLogFile("******************************MELDING***********************")
            End If

        Catch ex As Exception
            ' Return ex.Message
            OCDF.WriteLogFile(ex.Message & " in attachment.vb")
        End Try
        ' Return fileName & ".xml"

    End Sub

    Private Function OCDFgetDataLine(ByVal strDataLine As String)
        Dim arrDataLine As Array
        If My.Settings.debug = True Then
            OCDF.WriteLogFile("Vi er i getDataLine og vi har f�tt strDataLine: " & strDataLine)
        End If
        arrDataLine = Split(strDataLine, ";")

        Return arrDataLine
    End Function

    Private Function OCDFgetFromDataLine(ByVal arrDataLine As Array, ByVal strField As String)

        ' This function shall return the datafield that the user has asked for
        ' The idea is that we get the command from strField and we check this against the
        ' array that we have. If we find the command the array, we return the value. 
        Dim ar As Integer
        ar = UBound(arrDataLine)
        Dim a As Integer
        If My.Settings.debug = True Then
            OCDF.WriteLogFile("Dette henter vi ut av arrDataLine: ")
        End If
        Dim strTemp As String
        For a = 0 To ar
            ' Dim strTemp As String = arrDataLine(a).ToString
            ' Select Case (strTemp.StartsWith(strField))

            strTemp = arrDataLine(a).ToString

            If strTemp.StartsWith(strField) Then
                If strField.ToString = "n" Then
                    If My.Settings.debug = True Then
                        OCDF.WriteLogFile("Command: " & strField & " returns: " & arrDataLine(a + 1).ToString)
                    End If
                    Return arrDataLine(a + 1).ToString
                End If
                If My.Settings.debug = True Then
                    OCDF.WriteLogFile("Command: " & strField & " returns: " & Mid(strTemp, strField.Length + 1))
                End If
                Return Mid(strTemp, strField.Length + 1)
            End If

        Next a

        Return ""

    End Function
    Private Function OCDFGetParamId(ByVal strAlive As String) As String
        ' -----------------------------------------------------------------------------
        ' OCDFHeartBeat
        '
        ' Description:
        '   This subroutine disconnects from the NNTP-server.
        '
        ' Parameters:
        '   passWord = The password is needed in order to run the command... 
        '
        ' Returns:
        '   Nothing
        '   
        ' Notes :
        '   It looks like the password could be taken out of the routine.
        '   none
        ' -----------------------------------------------------------------------------

        Dim hbID As String
        hbID = Mid(strAlive, strAlive.IndexOf(";") + 2, strAlive.IndexOf(";pid=") - strAlive.IndexOf(";") - 1)
        If My.Settings.debug = True Then
            OCDF.WriteLogFile("Vi har f�tt ut f�lgende fra strengen: " & hbID)
        End If

        Return hbID

    End Function
    Public Sub OCDFDisconnect()
        ' -----------------------------------------------------------------------------
        ' Disconnect
        '
        ' Description:
        '   This subroutine disconnects from the NNTP-server.
        '
        ' Parameters:
        '   passWord = The password is needed in order to run the command... 
        '
        ' Returns:
        '   Nothing
        '   
        ' Notes :
        '   It looks like the password could be taken out of the routine.
        '   none
        ' -----------------------------------------------------------------------------
        ' Disconnects from the NNTP-server
        Dim commandData As String
        Dim contentBuffer() As Byte
        Dim responseString As String

        ' Tell the server that we are through
        OCDF.WriteLogFile("Sender S_LOGOFF_REQ kommandoen til server")
        Try

            commandData = "S_LOGOFF_REQ" & vbCrLf
            contentBuffer = System.Text.Encoding.ASCII.GetBytes(commandData.ToCharArray())
            CommandSender.Write(contentBuffer, 0, contentBuffer.Length)
        Catch ex As System.Net.Sockets.SocketException
            OCDF.WriteLogFile("***** ERROR: Disconnect *****")
            OCDF.WriteLogFile("Error happened in OCDF.vb Disconnect: Connection failure: " & ex.Message)
            OCDF.WriteLogFile("Socket errorcode: " & ex.SocketErrorCode.ToString)
            OCDF.WriteLogFile("Native errorcode: " & ex.NativeErrorCode.ToString)
            If Not ex.InnerException Is Nothing Then
                OCDF.WriteLogFile("Inner Exception Message: " & ex.InnerException.Message)
            End If

        Catch exs As Exception
            OCDF.WriteLogFile("Normal Exception connection failure (in nntp.vb Disconnect): " & exs.Message)
            If Not exs.InnerException Is Nothing Then
                OCDF.WriteLogFile("Inner Exception Message: " & exs.InnerException.Message)
            End If

            Return
        End Try

        responseString = ContentReceiver.ReadLine()



        OCDF.WriteLogFile(responseString)

        ContentReceiver.Close()
        CommandSender.Close()
        OCDFServer.Close()

        OCDF.WriteLogFile("Sendt S_LOGOFF_REQ-kommando til serveren")

    End Sub

    Public Sub OCDFClose()
        ContentReceiver.Close()
        CommandSender.Close()
        OCDFServer.Close()
    End Sub

End Class
