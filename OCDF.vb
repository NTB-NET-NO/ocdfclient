Imports System.ServiceProcess
Imports System
Imports System.IO
Imports system.Net.Sockets
Imports system.text
Imports System.Net
Imports System.Collections.Specialized
Imports System.Configuration
Imports System.Configuration.ConfigurationSettings
Imports System.Timers
Imports ntb_FuncLib
Imports System.Xml
Imports System.Xml.Serialization
Public Class OCDF

    ' Inherits System.ServiceProcess.ServiceBase

    ' Henter variabler som skal brukes 
    Public Shared logFilePath As String = My.Settings.logFilePath '("logFilePath")
    Public Shared lFileName As String = My.Settings.logFileName ' ("logFileName")
    Private Shared today As Date = Now
    Private Shared yearnow As Integer
    Private Shared monthnow As Integer
    Private Shared daynow As Integer
    Private Shared timenow As Integer
    Public Shared datefilename As String
    Public Shared lastArticleNum As Integer
    Public Shared lastArticleContainer As ArrayList
    Public Shared lastArticleGroup As ArrayList


    Public Shared logFileName As String
    Public Shared OutPath As String = My.Settings.OutPath
    Public Shared HomePath As String = My.Settings.HomeDir
    Public Shared pollInterval As Integer = My.Settings.PollingInterval
    Public Shared NewsServer As String = My.Settings.Server ' ("APNewsServer")
    Public Shared dirRoot As String = My.Settings.RootDir '("APRoot")
    Public Shared RestartInterval As Integer = My.Settings.Restart

    Public doDisconnect As Boolean = False

    Private strDayTime As DateTime

    Private OCDFConnection As classOCDF = Nothing
    ' Private NNTPAttachment As Attachment = Nothing

    Private XMLfile As ClassXML = Nothing

    Private uName As String = My.Settings.Username
    Private pWord As String = My.Settings.Password

    Private contentReceiver As StreamReader

    Private xmlConfig As New XmlDocument

    ' To get Configinformation
    Public filepath As String
    Public filename As String
    Public groups As New System.Collections.ArrayList
    Public FilesDelete As Integer

    Public Shared xmlStateFile As String = My.Settings.StateFile
    Public Shared xmlStateFileExists As Boolean = False

    Public Shared appPath As String = My.Application.Info.DirectoryPath

    ' Variables related to errors
    Public Shared ErrorPath As String = My.Settings.ErrorPath
    Public Shared ErrorFileName As String

    Public Shared FromStart As Boolean = False

    ' Debug true/false
    Public Shared doDebug As Boolean = My.Settings.debug

    Protected Sub init()
        ' -----------------------------------------------------------------------------
        ' init
        '
        ' Description:
        '   This routine is called when the application starts. 
        '   It reads all the values from the app.config-file and stores it
        '   in strings, arrays and integers
        '   It also makes sure that the application is not connected to the News server
        '
        ' Parameters:
        '
        ' Returns:
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------

        Dim OCDFevents As New EventLog
        Dim OCDFeventsType As New EventLogEntryType

        Try
            CreateOutDirectories()
        Catch ex As Exception
            ' ocdfevents.
            ' MsgBox(ex.ToString)
        End Try

        

        If doDebug = True Then
            ' ocdfevents.
            WriteLogFile("******************************************************************************")
            WriteLogFile("*                                                                            *")
            WriteLogFile("*                                                                            *")
            WriteLogFile("*           START P� OCDFCLIENT                                              *")
            WriteLogFile("*                                                                            *")
            WriteLogFile("*                                                                            *")
            WriteLogFile("*                                                                            *")
            WriteLogFile("******************************************************************************")
            WriteLogFile("Init OCDFclient")
        End If

        Dim XMLfile As ClassXML = New ClassXML
        ' Dim addGroups As ArrayList


        

        If doDebug = True Then
            WriteLogFile("Checking if " & appPath & "\" & xmlStateFile & " exists")
        End If

        Dim xmlStateFileExists As Boolean = False
        If (My.Computer.FileSystem.FileExists(appPath & "\" & xmlStateFile)) Then
            xmlStateFileExists = True
            XMLfile.readXML()
        Else
            WriteLogFile(appPath & "\" & xmlStateFile & " finnes ikke!")
            xmlStateFileExists = False

        End If

        If doDebug = True Then
            WriteLogFile("Done checking for file exists!")
        End If






        If xmlStateFileExists = False Then
            If doDebug = True Then
                WriteLogFile("Starter WriteStateXMLFile existerte ikke, s� vi lager den")
            End If

            XMLfile.WriteOCDFStateFile()
            XMLfile.WriteOCDFStateToXmlFile("0", "0")

            If doDebug = True Then
                WriteLogFile("Ferdig med WriteStateXMLFile")
            End If

        End If

 



        uName = My.Settings.Username
        pWord = My.Settings.Password

        If doDebug = True Then
            WriteLogFile("Lagt til brukernavn og passord: " & uName & "/" & pWord)
        End If


        ' Dim apGroups() As String = {"ap.ds.dsf.all", "ap.european.news", "ap.headline.headlines.top"}
        Dim PollingGetTime As Integer = My.Settings.PollingInterval ' Sets receive to 30 seconds or whatever is in the config-file.
        Dim ErrorRetry As Integer = My.Settings.ErrorRetryTime ' sets receive to 10 seconds on error

        ' kobler oss ned for � v�re sikker p� at vi ikke er tilkoblet.
        'If doDebug = True Then
        '    If doDebug = True Then WriteLogFile("Kobler fra serveren")
        'End If

        'If (OCDFConnection IsNot Nothing) Then
        '    Try
        '        OCDFConnection.Disconnect(pWord)
        '    Catch ex As System.Net.Sockets.SocketException
        '        WriteLogFile("Error happened in init - OCDFConnection.Disconnect")
        '        WriteLogFile("Connection failure: " & ex.Message)
        '        WriteLogFile("Socket errorcode: " & ex.SocketErrorCode.ToString)
        '        WriteLogFile("Native errorcode: " & ex.NativeErrorCode.ToString)
        '        If Not ex.InnerException Is Nothing Then
        '            WriteLogFile("Inner Exception Message: " & ex.InnerException.Message)
        '        End If
        '    Catch exs As Exception
        '        WriteLogFile("Normal Exception connection failure: " & exs.Message & " In init")
        '        If Not exs.InnerException Is Nothing Then
        '            WriteLogFile("Inner Exception Message: " & exs.InnerException.Message & " In Init")
        '        End If
        '        OCDFConnection = Nothing
        '        Return
        '    End Try
        'End If

        If doDebug = True Then
            WriteLogFile("Done init")
        End If




    End Sub


    Public Shared Function GetDate()
        ' -----------------------------------------------------------------------------
        ' GetDate
        '
        ' Description:
        '   This subroutine Gets todays date and returns it
        '   
        '
        ' Parameters:
        '
        ' Returns:
        '   todays date
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        Dim returnDate As String = Nothing
        today = Now
        yearnow = today.Year
        monthnow = today.Month
        daynow = today.Day

        Dim minutes As String = today.ToString("mm")


        ' timenow = Convert.ToString(today.AddMinutes(6))
        returnDate = yearnow.ToString & monthnow.ToString & daynow.ToString

        Return returnDate
    End Function

    Public Shared Function CreateLogFileName()
        ' -----------------------------------------------------------------------------
        ' CreateLogFileName
        '
        ' Description:
        '   This subroutine creates the logfile-filename.
        '   
        '
        ' Parameters:
        '
        ' Returns:
        '   Returns the logfilename
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        datefilename = Nothing
        datefilename = GetDate.ToString
        CreateLogFileName = logFilePath & "\" & datefilename & lFileName

        Return CreateLogFileName
    End Function

    Public Shared Function CreateErrorLogFileName()
        ' -----------------------------------------------------------------------------
        ' CreateErrorLogFileName
        '
        ' Description:
        '   This subroutine creates the error logfile name
        '   
        '
        ' Parameters:
        '
        ' Returns:
        '   The error log file name
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        Dim strDate As String = GetDate.ToString
        Trace.Write(strDate)
        Trace.Write(ErrorPath)

        CreateErrorLogFileName = ErrorPath & "\" & "error-" & strDate & ".log"
        Trace.Write(CreateErrorLogFileName)

        Return CreateErrorLogFileName
    End Function
    Public Sub New()

        ' MyBase.New()
        logFileName = Nothing
        logFileName = CreateLogFileName()

        init()

        ' This call is required by the Windows Form Designer.
        ' WriteLogFile("I New og starter initialize component!")
        Try
            InitializeComponent()
        Catch ex As Exception
            WriteErrorLogFile("Initialize component failed:", ex.ToString)
        End Try

        Try
            PollTimer.Start()
        Catch ex As Exception
            WriteErrorLogFile("PollTimer Start failed:", ex.ToString)

        End Try






    End Sub
    Public Shared Sub WriteErrorLogFile(ByVal strMessage As String, ByVal strException As String)
        ' -----------------------------------------------------------------------------
        ' WriteErrorLogFile
        '
        ' Description:
        '   This subroutine writes to the error logfile.
        '   
        '
        ' Parameters:
        '   strMessage = The message that is to be put in the error log file.
        '   strException = The Exeption the error trew. 
        '
        ' Returns:
        '   Nothing.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        ErrorFileName = CreateErrorLogFileName()
        Trace.Write(ErrorFileName.ToString)
        LogFile.WriteDebug(ErrorFileName, "Error: " & DateTime.Now())
        LogFile.WriteDebug(ErrorFileName, strMessage)
        LogFile.WriteDebug(ErrorFileName, strException)
    End Sub

    Public Shared Sub WriteLogFile(ByVal StrMessage As String)
        ' -----------------------------------------------------------------------------
        ' PollTimer_Elapsed
        '
        ' Description:
        '   
        '   
        '
        ' Parameters:
        '   
        '   
        '
        ' Returns:
        '   Nothing.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        logFileName = Nothing
        logFileName = CreateLogFileName()
        LogFile.WriteDebug(logFileName, StrMessage)
    End Sub



    Private Sub PollTimer_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles PollTimer.Disposed
        ' -----------------------------------------------------------------------------
        ' PollTimer_Elapsed
        '
        ' Description:
        '   
        '   
        '
        ' Parameters:
        '   
        '   
        '
        ' Returns:
        '   Nothing.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        If doDebug = True Then
            WriteLogFile("I PollTimer_Disposed.")
        End If

    End Sub

    Sub CreateOutDirectories()
        ' -----------------------------------------------------------------------------
        ' CreateOutDirectories
        '
        ' Description:
        '   This routine creates directories. 
        '   This routine might be removed as we can use a function in logfile2.vb
        '   that does the same.
        '
        ' Parameters:
        '   HomePath = this creates the root-directory if it does not exists
        '   logFilePath = The path to the directory where the logfiles shall be placed
        '   OutPath = The directory where the XML-messages shall be stored
        '   ErrorPath = The directory where errormessages (number of the article that was not received is stored)
        '
        ' Returns:
        '   Nothing
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        If doDebug = True Then
            '    WriteLogFile("In CreateOutDirectories")
        End If

        Dim dirs() As String = {dirRoot, HomePath, logFilePath, OutPath, ErrorPath}
        Dim a As Integer

        For a = 0 To dirs.Length - 1

            If Not Directory.Exists(dirs(a)) Then
                Try
                    Directory.CreateDirectory(dirs(a))
                Catch dex As DirectoryNotFoundException
                    ' MsgBox(dex.ToString)
                End Try



                ' Vi kan ikke skrive til noe som ikke eksisterer.
                ' WriteLogFile("Created: " & dirs(a))
            Else
                ' WriteLogFile(dirs(a) & " exists")
            End If

        Next a
        If doDebug = True Then
            ' WriteLogFile("Ferdig med � lage directories")
        End If


    End Sub

    Public Sub pause(ByVal pausetime As Integer)
        ' -----------------------------------------------------------------------------
        ' pause
        '
        ' Description:
        '   This routine is used to pause the number of seconds stated in the app.config file
        '
        ' Parameters:
        '   pausetime = The number of seconds the application should pause
        '
        ' Returns:
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        ' New pause routine since the old one stops at 00.00

        If doDebug = True Then WriteLogFile("Pause sub starts")


        Dim ticksBefore As Long
        Dim ticksAfter As Long
        Dim tickSeconds As Double

        ' time the user
        ticksBefore = Now.Ticks
        Do While (Now.Ticks - ticksBefore) / 10000000.0 < pausetime
            ' looping just to get a pause
        Loop
        ticksAfter = Now.Ticks

        tickSeconds = (ticksAfter - ticksBefore) / 10000000.0


        If doDebug = True Then WriteLogFile("Ventet i " & tickSeconds.ToString & " sekunder")

    End Sub

    Private Sub PollTimer_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles PollTimer.Elapsed
        ' -----------------------------------------------------------------------------
        ' PollTimer_Elapsed
        '
        ' Description:
        '   
        '   
        '
        ' Parameters:
        '   
        '   
        '
        ' Returns:
        '   Nothing.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        'Stop events while handling files
        ' Lager ny logfil - dersom det trengs. 
        logFileName = Nothing
        logFileName = CreateLogFileName()

        OCDFConnection = New classOCDF
        If doDebug = True Then WriteLogFile("I PollTimer_Elapsed.")
        If doDebug = True Then WriteLogFile("Navnet p� logfile: " & logFileName)
        PollTimer.Enabled = False

        'Public Shared lastArticleNum As Integer
        'Public Shared lastArticleContainer As ArrayList
        'Public Shared lastArticleGroup As ArrayList
        'Do getting news messages
        If doDebug = True Then WriteLogFile("Henter nyhetsmeldinger.")

        Try
            OCDFConnection.OCDFConnect(My.Settings.Server)
        Catch ex As Exception

        End Try
        Try
            ' In a NNTP-protocol you get the list then you get the
            ' articles.
            ' in OCDF you just send the REALTIME commando and you get the
            ' articles line by line. 
            OCDFConnection.OCDFConnect(My.Settings.Server)
            OCDFConnection.OCDFLogon()
            OCDFConnection.OCDFFixedDataReceive()
            OCDFConnection.OCDFMessageReceive()
        Catch ex As System.Net.Sockets.SocketException
            WriteErrorLogFile("Error happened in PollTimer_Elapsed during getListMain", ex.Message)
            WriteErrorLogFile("Socket errorcode: ", ex.SocketErrorCode.ToString)
            WriteErrorLogFile("Native errorcode: ", ex.NativeErrorCode.ToString)
            If Not ex.InnerException Is Nothing Then
                WriteErrorLogFile("Inner Exception Message: ", ex.InnerException.Message)
            End If
        Catch exs As Exception
            Trace.Write("********************** EXCEPTION CAUGHT! *******************************")
            WriteErrorLogFile("Normal Exception connection failure: ", exs.Message)
            If Not exs.InnerException Is Nothing Then
                WriteErrorLogFile("Inner Exception Message: ", exs.InnerException.Message)
            End If
            OCDFConnection = Nothing

        End Try


        
        If doDebug = True Then WriteLogFile("Kobler oss av serveren")

        If OCDFConnection IsNot Nothing Then
            Try
                If OCDFConnection.serviceRunning = True Then


                    OCDFConnection.OCDFDisconnect()
                End If

            Catch ex As System.Net.Sockets.SocketException
                WriteErrorLogFile("Error happened in PollTimer_Elapsed NNTPConnection.Disconnect. Connection failure: ", ex.Message)
                WriteErrorLogFile("Socket errorcode: ", ex.SocketErrorCode.ToString)
                WriteErrorLogFile("Native errorcode: ", ex.NativeErrorCode.ToString)
                If Not ex.InnerException Is Nothing Then
                    WriteErrorLogFile("Inner Exception Message: ", ex.InnerException.Message)
                End If

            Catch exs As Exception
                WriteErrorLogFile("Normal Exception connection failure: ", exs.Message)
                If Not exs.InnerException Is Nothing Then
                    WriteErrorLogFile("Inner Exception Message: ", exs.InnerException.Message)
                End If
            End Try
        End If
        'Restart events

        PollTimer.Interval = pollInterval * 1000
        PollTimer.Enabled = True

        If doDebug = True Then WriteLogFile("Avslutter henting av nyhetsmeldinger.")



    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        logFileName = Nothing
        logFileName = CreateLogFileName()


        FromStart = True
        ' init()
        ' If doDebug = True Then WriteLogFile("Vi er i OnStart og vi starter init")
        PollTimer.Enabled = True

        WriteLogFile("EXE-filen ligger " & appPath)
        WriteLogFile("Kildekoden ligger: \\FilPrint\thu$\Visual Studio Projects\OCDFclient\bin\Release")
        WriteLogFile("In OnStart.")


        WriteLogFile("OCDF-client Service Startet.")
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        ' Add code here to perform any tear-down necessary to stop your service.
        If doDebug = True Then LogFile.WriteLog(logFileName, "In OnStop.")

        If Not OCDFConnection Is Nothing Then


            Try
                OCDFConnection.serviceRunning = False
                OCDFConnection.OCDFDisconnect() ' Kobler oss av server. 
            Catch ex As System.Net.Sockets.SocketException
                WriteErrorLogFile("Error happened in OnStop OCDF.Disconnect. Connection failure: ", ex.Message)
                WriteErrorLogFile("Socket errorcode: ", ex.SocketErrorCode.ToString)
                WriteErrorLogFile("Native errorcode: ", ex.NativeErrorCode.ToString)
                If Not ex.InnerException Is Nothing Then
                    WriteErrorLogFile("Inner Exception Message: ", ex.InnerException.Message)
                End If

            Catch exs As Exception
                WriteErrorLogFile("Normal Exception connection failure: ", exs.Message)
                If Not exs.InnerException Is Nothing Then
                    WriteErrorLogFile("Inner Exception Message: ", exs.InnerException.Message)
                End If


                Return
            End Try
        End If


        PollTimer.Enabled = False
        LogFile.WriteLog(logFileName, "OCDF-client stopped.")
    End Sub
End Class
